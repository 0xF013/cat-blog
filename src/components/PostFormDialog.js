import React from 'react'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import Snackbar from '@material-ui/core/Snackbar';
import DialogTitle from '@material-ui/core/DialogTitle'
import { compose, withHandlers, withState, lifecycle } from 'recompose'
import { API, graphqlOperation } from 'aws-amplify'

const PostFormDialog = ({
  isOpen,
  onClose,
  onSubmit,
  id,
  title,
  setTitle,
  body,
  setBody,
  loading, error, setError
}) => (
  <Dialog open={isOpen} onClose={onClose} aria-labelledby='form-dialog-title'>
    <DialogTitle id='form-dialog-title'>
      {id ? 'Update' : 'Create'} Post
    </DialogTitle>
    <form action='' onSubmit={e => {e.preventDefault(); onSubmit()}}>
      <DialogContent>
        <TextField
          required
          disabled={loading}
          autoFocus
          margin='dense'
          label='Title'
          type='text'
          fullWidth
          value={title}
          onChange={e => setTitle(e.target.value)}
        />
        <TextField
          required
          disabled={loading}

          margin='dense'
          label='Body (min 100 chars)'
          type='text'
          fullWidth
          multiline
          rows='10'
          value={body}
          onChange={e => setBody(e.target.value)}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose} color='primary'>
          Cancel
        </Button>
        <Button type='submit' color='primary' disabled={loading}>
          Save
        </Button>
      </DialogActions>
    </form>
    <Snackbar
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'left',
      }}
      open={error}
      autoHideDuration={6000}
      onClose={() => setError(false)}
      ContentProps={{
        'aria-describedby': 'message-id',
      }}
      message={<span id="message-id">{error}</span>}
    />    
  </Dialog>
)

const createMutation = `
  mutation CreateBlogPost($input: CreateBlogPostInput!) {
    createBlogPost(input: $input) {
      id
      title
      body
      createdAt
    }
  }
`

const updateMutation = `
  mutation UpdateBlogPost($input: UpdateBlogPostInput!) {
    updateBlogPost(input: $input) {
      id
      title
      body
      createdAt
    }
  }
`

export default compose(
  withState('title', 'setTitle', ''),
  withState('id', 'setId'),
  withState('body', 'setBody', ''),
  withState('loading', 'setLoading', false),
  withState('error', 'setError', false),  
  lifecycle({
    componentDidMount () {
      if (this.props.item) {
        this.props.setId(this.props.item.id)
        this.props.setTitle(this.props.item.title)
        this.props.setBody(this.props.item.body)
      }
    }
  }),
  withHandlers({
    onSubmit: (props) => async () => {
      const {id, title, body} = props
      if (body.length < 100) {
        props.setError('Body is too short')
        return
      }
      try {
        props.setLoading(true)
        props.setError(false)

        const input = id ? {
          id,
          title,
          body,
        } : {
          title,
          body,
          createdAt: (new Date()).toISOString()
        }

        await API.graphql(
          graphqlOperation(props.id ? updateMutation : createMutation, {
            input
          })
        )
        props.setLoading(false)
        props.onClose()

      } catch (e) {
        props.setLoading(false)
        props.setError('Error creating blog post. Check that data is valid')
      }
    }
  })
)(PostFormDialog)
