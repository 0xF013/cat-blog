import React from 'react'
import { API, graphqlOperation } from 'aws-amplify'
import { Connect } from 'aws-amplify-react'
import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
import Pagination from 'material-ui-flat-pagination'
import { compose, withState, withHandlers } from 'recompose'
import PostFormDialog from './PostFormDialog'
import PostImageDialog from './PostImageDialog'

import { S3Image } from 'aws-amplify-react';

const query = `
  query ListBlogPosts($limit:Int, $nextToken:String) {
    listBlogPosts(limit:$limit, nextToken: $nextToken) {
      items {
        id
        title
        body
        createdAt
      }
      nextToken
    }
  }
`

const subscription = `
subscription subscribeToEvents {
  onCreateBlogPost {
    id
    title
    body
    createdAt
  }
  onUpdateBlogPost {
    title
    body
  }  
  onDeleteBlogPost {
    title
    body
  }    
}
`

const byDate = (a, b) => {
  return new Date(a.createdAt) > new Date(b.createdAt) ? -1 : 1
}

const PostList = ({ offset, setOffset, editId, setEditId, deleteItem, editImageId, setEditImageId, limit = 5 }) => (
  <Connect
    query={graphqlOperation(query, { limit: 10000 })}
    subscription={graphqlOperation(subscription)}
    onSubscriptionMsg={(prev, { onCreateBlogPost, onUpdateBlogPost, onDeleteBlogPost }) => {
      console.log(prev, onCreateBlogPost)
      if (onCreateBlogPost) {
        return {
          ...prev,
          listBlogPosts: {
            ...prev.listBlogPosts,
            items: [...prev.listBlogPosts.items, onCreateBlogPost]
          }
        }
      }

      if (onUpdateBlogPost) {
        const arrayWithoutItem = prev.listBlogPosts.items.filter(item => item.id !== onUpdateBlogPost.id)
        return {
          ...prev,
          listBlogPosts: {
            ...prev.listBlogPosts,
            items: [...arrayWithoutItem, onUpdateBlogPost]
          }
        }
      }      

      if (onDeleteBlogPost) {
        return {
          ...prev,
          listBlogPosts: {
            ...prev.listBlogPosts,
            items: prev.listBlogPosts.items.filter(item => item.id !== onDeleteBlogPost.id)
          }
        }
      }        

      return prev
    }}
  >
    {({ data, loading, error }) => {
      if (error) return <h3>Error</h3>
      if (loading || !data) return <h3>Loading...</h3>
      return (
        <React.Fragment>
          <Grid
            container
            justify='center'
            spacing={24}
            style={{ padding: '24px' }}
          >
            {data.listBlogPosts.items
              .sort(byDate)
              .slice(offset, offset + limit)
              .map(item => (
                <Grid item style={{ width: '900px' }}>
                  <Card>
                    <CardActionArea>
                      <CardContent>
                        {item.id !== editImageId && <S3Image imgKey={item.id} />}
                        <Typography gutterBottom variant='h5' component='h2'>
                          {item.title}
                        </Typography>
                        <Typography variant='subheading'>
                          {(new Date(item.createdAt)).toLocaleString()}
                        </Typography>                        
                        <Typography component='p'>
                          {item.body}
                        </Typography>
                      </CardContent>
                    </CardActionArea>
                    <CardActions>
                      <Button onClick={() => setEditId(item.id)}>Edit</Button>
                      <Button onClick={() => deleteItem(item.id)}>Delete</Button>
                      <Button onClick={() => setEditImageId(item.id)}>Edit Image</Button>
                    </CardActions>
                  </Card>
                </Grid>
              ))}
            <Grid item>
              <Pagination
                style={{ width: '900px' }}
                limit={limit}
                offset={offset}
                total={data.listBlogPosts.items.length}
                onClick={(e, offset) => setOffset(offset)}
              />
            </Grid>
            {editId &&
              <PostFormDialog
                item={data.listBlogPosts.items.find(item => item.id === editId)}
                isOpen
                onClose={() => setEditId(undefined)}
              />}
            {editImageId &&
              <PostImageDialog
                id={editImageId}
                isOpen
                onClose={() => setEditImageId(undefined)}
              />}              
          </Grid>

        </React.Fragment>
      )
    }}
  </Connect>
)

const deleteMutation = `
  mutation DeleteBlogPost($input: DeleteBlogPostInput!) {
    deleteBlogPost(input: $input) {
      id
    }
  }
`

export default compose(
  withState('offset', 'setOffset', 0),
  withState('editId', 'setEditId'),
  withState('editImageId', 'setEditImageId'),
  withHandlers({
    deleteItem: props => async id => {
      await API.graphql(
        graphqlOperation(deleteMutation, {input: {id}})
      )
    }
  })
)(PostList)
