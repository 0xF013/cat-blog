import React from 'react'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import { S3Image } from 'aws-amplify-react';

const PostImageDialog = ({
  isOpen,
  onClose,
  id,
}) => (
  <Dialog open={isOpen} onClose={onClose} aria-labelledby='form-dialog-title'>
    <DialogTitle id='form-dialog-title'>
      Edit Image
    </DialogTitle>
    <DialogContent>
      <S3Image imgKey={id} picker />
    </DialogContent>
    <DialogActions>
      <Button onClick={onClose} color='primary'>
        Close
      </Button>
    </DialogActions>
  </Dialog>
)

export default PostImageDialog

