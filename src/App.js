import React from 'react'
import Amplify, { Storage, Auth } from 'aws-amplify'
import aws_exports from './aws-exports'
import { withAuthenticator } from 'aws-amplify-react'
import Button from '@material-ui/core/Button'
import CssBaseline from '@material-ui/core/CssBaseline'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import AddIcon from '@material-ui/icons/Add'
import { withStyles } from '@material-ui/core/styles'
import { compose, withState } from 'recompose'
import PostFormDialog from './components/PostFormDialog'
import PostList from './components/PostList'

Amplify.configure(aws_exports)
Storage.configure({ level: 'private' })

const styles = theme => ({
  fab: {
    position: 'fixed',
    bottom: theme.spacing.unit * 2,
    right: theme.spacing.unit * 2
  }
})


const App = ({ classes, isDialogOpen, setDialogOpen }) => (
  <div style={{backgroundColor: '#eeeeee'}}>
    <CssBaseline />
    <AppBar position='static'>
      <Toolbar>
        <Typography variant='h6' color='inherit' style={{flexGrow: '1'}}>
          Cats Blog
        </Typography>
        <Button color="inherit" onClick={() => Auth.signOut().then(() => window.location.reload())}>Logout</Button>
      </Toolbar>
    </AppBar>
    <PostList />
    <Button
      variant='fab'
      className={classes.fab}
      color='primary'
      onClick={() => setDialogOpen(true)}
    >
      <AddIcon />
    </Button>
    {isDialogOpen && <PostFormDialog
      title=''
      body=''
      isOpen
      onClose={() => setDialogOpen(false)}
    />}
  </div>
)

export default compose(
  withAuthenticator,
  withState('isDialogOpen', 'setDialogOpen', false),
  withStyles(styles)
)(App)
